from django.shortcuts import render
from backend import *
from operator import itemgetter

# Create your views here.

def home(request):
	user_token = request.session.get('user_token')
	response_data = {}
	response_data['nTweets'] = howManyWithOutNot()

	response = render(request, 'index.html', response_data)
	
	return response

def userProfile(request, username):

 	user_token = request.session.get('user_token')
	response_data = {}
	response_data['chart1_value1'] = 100
	response_data['chart1_value2'] = 200
	response_data['chart1_value3'] = 100
	response_data['chart1_value4'] = 50
	response_data['chart1_value5'] = 25
	response_data['chart2_value1'] = 50
	response_data['chart2_value2'] = 50
	response_data['chart2_value3'] = 25
	response_data['chart2_value4'] = 75
	response_data['chart2_value5'] = 100
	response_data['chart3_value1'] = 40
	response_data['chart3_value2'] = 30
	response_data['chart3_value3'] = 90
	response_data['chart3_value4'] = 50
	response_data['chart3_value5'] = 25

	response_data['NAlltweets'] = howManyTotalTweets(username)
	response_data['Ntweets'] = howManyBadTweets(username)
	response_data['alltweets'] = getBadTweetsFromUser(username)
	response_data['name'] = username

	response = render(request, 'userprofile.html', response_data)
	return response

def statistics(request):

 	user_token = request.session.get('user_token')
	response_data = {}
	response_data['value'] = 100

	response = render(request, 'statistics.html', response_data)
	return response

def alerts(request):

 	user_token = request.session.get('user_token')
	people = {}

	#clearNotification(howMany())

	listUser = listUsers()

	for user in listUser:
		people[user] = howManyBadTweets(user)

	people = sorted(people.items(), key=itemgetter(1), reverse=True)

	response_data = {}
	response_data['people'] = people

	response = render(request, 'alerts.html', response_data)
	return response

def login(request):

 	user_token = request.session.get('user_token')
	response_data = {}

	response = render(request, 'login.html', response_data)
	return response

def notification(request):

 	user_token = request.session.get('user_token')
	response_data = {}
	response_data['value'] = 100

	response = render(request, 'notification.html', response_data)
	return response