# -*- coding: utf-8 -*-

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy import API
import re
from pymongo import MongoClient
import json
import sys
import time

# Go to http://apps.twitter.com and create an app.
# The consumer key and secret will be generated for you after
consumer_key="TjSqoO08gd7xuG3OvUWPJw"
consumer_secret="kyn0ZF1Oohm79Y856p83vqYXNqwKTcMsxcsnYTHonI"

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token="52850349-3rWHLqYA6wwQOeiETn2j3iGxvtp8oRnKpBnxlTEoI"
access_token_secret="28Ol1sPosHZgTpFFZrOcgDC3ZPJ0lVUivD9RHzKmI"


"""dict = ['suicidio',
'suicidarse',
'suicidarme',
'suicidate',
'muerte',
'morir',
'matarme',
'muerete',
'acoso',
'acosan',
'pegar',
'pega',
'pegale',
'pegame',
'pegarte',
'maricon',
'nenaza',
'cuernos',
'infiel',
'degollar',
'reventar',
'olvidar',
'olvidare',
'aguantar',
'aguantado',
'cielo'"""

dict=["te voy a matar",
      "eres un maricon",
      "eres un gilipollas",
      "mi vida no tiene sentido",
      "no tengo ganas de vivir",
      "mi vida es una mierda",
      "no aguanto mas",
      "echarme de menos",
      "no quiero ir al colegio",
      "no quiero ir al instituto",
      "os echare de menos",
      "eres un subnormal",
      "eres una puta",
      "eres una zorra",
      "eres una guarra",
      "te vas a enterar",
      "preparate para morir",
      "vete a limpiar",
      "vete a fregar",
      "chupamela",
      "vete a la cocina",
      "me voy a vengar",
      "esta vida no tiene sentido",
      "no me queda mas remedio",
      "puta zorra",
      "me has puesto los cuernos",
      "me has sido infiel",
      "eres un puto infiel"
      ]

dictEx=["es broma", "jajaj", "jajaja", "jiji", "jojo", "xd", ":)", "lol", "te amo", "te quiero","amor", "quererte"]

regex = re.compile('|'.join(dict).lower())
#regexEx = re.compile('|'.join(dictEx).lower())
linenum_re = re.compile(r'([A-Z][A-Z]\d+)')
retweets_re = re.compile(r'^RT\s')
mentionregex = re.compile(ur'@\w*')

enc = lambda x: x.encode("latin1", errors='ignore')

global notifications
notiications = 0
class StdOutListener(StreamListener):
    """ A listener handles tweets that are received from the stream.
    This is a basic listener that just prints received tweets to stdout.
    """
    def on_data(self, data):
        tweet = json.loads(data)

        if not tweet.has_key('user'):
            print ('No user data - ignoring tweet.')
            return True

        user = enc(tweet['user']['name'])
        text = enc(tweet['text'])

        # ignore text that doesn't contain one of the keywords
        matches = re.search(regex, text.lower())
        if not matches:
            return True

        # ignore retweet
        if re.search(retweets_re, text):
            return True
       # if re.search(regexEx.text.lower()):
       #     return True
        print text
        self.saveTweet(enc(data))
        return True

    def on_error(self, status):
        print(status)

    def saveTweet(self, data):
        try:
            client = MongoClient('localhost', 27017)
            db = client['twitter_db']
            collection = db['twitter_collection']
            collection.insert(json.loads(data))

            return True
        except BaseException, e:
            print 'failed ondata,', str(e)
            time.sleep(5)
            pass



def getBadTweetsFromUser(user):
    list =[]
    client = MongoClient('localhost', 27017)
    db = client['twitter_db']
    collection = db['twitter_collection']
    tweets = collection.find({u'user.screen_name' : user})
    for tweet in tweets:
        list.append(tweet)
    return list

def howManyBadTweets(user):
    count = 0
    client = MongoClient('localhost', 27017)
    db = client['twitter_db']
    collection = db['twitter_collection']
    tweets = collection.find({u'user.screen_name' : user})
    for tweet in tweets:
        count = count + 1
    return count

def howManyTotalTweets(user):
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = API(auth)

    # Get information about the user
    data = api.get_user(user)

    return data.statuses_count

def howManyWithOutNot():
    count = 0
    client = MongoClient('localhost', 27017)
    db = client['twitter_db']
    collection = db['twitter_collection']
    count = collection.count()
    return count

def howMany():
    global notifications
    count = 0
    client = MongoClient('localhost', 27017)
    db = client['twitter_db']
    collection = db['twitter_collection']
    count = collection.count()
    return count -  notifications

def clearNotification(count):
    global notifications
    notifications = notifications + count

def listUsers():
    list = []
    client = MongoClient('localhost', 27017)
    db = client['twitter_db']
    collection = db['twitter_collection']
    tweets = collection.distinct('user.screen_name')
    for tweet in tweets:
        list.append(tweet)
    return list

def startListen(l):
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, l)
    stream.filter(track=dict,async=True)


