from __future__ import unicode_literals

from django.apps import AppConfig


class SocialdrumConfig(AppConfig):
    name = 'socialdrum'
